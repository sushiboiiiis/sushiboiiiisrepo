﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class movement : MonoBehaviour
{
    public Vector2 speed;
    public int jumpHeight;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        //get input for movements.
        float inputX = Input.GetAxis("Horizontal");
        //float inputY = Input.GetAxis("Vertical");

        Vector2 movement = new Vector2(speed.x * inputX, 0);
        movement *= Time.deltaTime;
        transform.Translate(movement);

        //jump key movement
        if (Input.GetKeyDown("space"))
        {
            transform.Translate(Vector3.up * jumpHeight * Time.deltaTime, Space.World);
        }
    }
}
